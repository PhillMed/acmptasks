package id.tasks.from0001to0100;

// "Game"
// we have a number xyz
// y always 9
// x+z = y (9)
// we only need to accept the number x

import java.util.Scanner;

public class Task0004 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a number 1-9");

        int x = scanner.nextInt();
        while (x < 1 || x > 9) {
            System.out.println("invalid number. Enter a number from 1 to 9");
            x = scanner.nextInt();
        }

        System.out.println("Your number is " + x + "9" + (9 - x));
    }
}
