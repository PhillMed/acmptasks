package id.tasks.from0001to0100;

import java.util.Scanner;

/*
"More-less"
In this problem, it is necessary to compare two integers.
 */

public class Task0025 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        if (a > b) {
            System.out.println(">");
        } else if (a < b) {
            System.out.println("<");
        } else {
            System.out.println("=");
        }
    }
}
