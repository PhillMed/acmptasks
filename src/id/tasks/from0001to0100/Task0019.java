package id.tasks.from0001to0100;

/*
"Queen, Rook and Knight"
There are three pieces on an 8x8 chessboard(A-H 1-8): a queen, a rook and a knight.
It is required to determine the number of empty fields of the board that are under the battle.
chess pieces can "beat" through other pieces.
in: separated by a space, the coordinates of the location of three pieces: the queen, the rook and the knight, respectively.
out: the number of empty fields that are under the battle.
 */

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Task0019 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String queen = sc.next();
        String rook = sc.next();
        String knight = sc.next();
        int count = countAttackedSquares(queen, rook, knight);
        System.out.println(count - 3);
    }

    public static int countAttackedSquares(String queen, String rook, String knight) {
        Set<String> attackedSquares = new HashSet<>();
        attackedSquares.addAll(getAttackedSquaresByQueen(queen));
        attackedSquares.addAll(getAttackedSquaresByRook(rook));
        attackedSquares.addAll(getAttackedSquaresByKnight(knight));
        return attackedSquares.size();
    }

    public static Set<String> getAttackedSquaresByQueen(String queen) {
        Set<String> attackedSquares = new HashSet<>();
        char column = queen.charAt(0);
        int row = Integer.parseInt(queen.substring(1));
        for (int i = 1; i <= 8; i++) {
            attackedSquares.add(column + "" + i);
            attackedSquares.add((char) (i + 'A' - 1) + "" + row);
            if (column - i + row >= 1 && column - i + row <= 8) {
                attackedSquares.add((char) (column - i) + "" + (row + i));
            }
            if (column + i - row >= 'A' && column + i - row <= 'H') {
                attackedSquares.add((char) (column + i) + "" + (row - i));
            }
        }
        attackedSquares.remove(queen);
        return attackedSquares;
    }

    public static Set<String> getAttackedSquaresByRook(String rook) {
        Set<String> attackedSquares = new HashSet<>();
        char column = rook.charAt(0);
        int row = Integer.parseInt(rook.substring(1));
        for (int i = 1; i <= 8; i++) {
            attackedSquares.add(column + "" + i);
            attackedSquares.add((char) (i + 'A' - 1) + "" + row);
        }
        attackedSquares.remove(rook);
        return attackedSquares;
    }

    public static Set<String> getAttackedSquaresByKnight(String knight) {
        Set<String> attackedSquares = new HashSet<>();
        char column = knight.charAt(0);
        int row = Integer.parseInt(knight.substring(1));
        if (row + 2 <= 8 && column - 1 >= 'A') {
            attackedSquares.add((char) (column - 1) + "" + (row + 2));
        }
        if (row + 2 <= 8 && column + 1 <= 'H') {
            attackedSquares.add((char) (column + 1) + "" + (row + 2));
        }
        if (row + 1 <= 8 && column - 2 >= 'A') {
            attackedSquares.add((char) (column - 2) + "" + (row + 1));
        }
        if (row + 1 <= 8 && column + 2 <= 'H') {
            attackedSquares.add((char) (column + 2) + "" + (row + 1));
        }
        if (row - 2 >= 1 && column - 1 >= 'A') {
            attackedSquares.add((char) (column - 1) + "" + (row - 2));
        }
        if (row - 2 >= 1 && column + 1 <= 'H') {
            attackedSquares.add((char) (column + 1) + "" + (row - 2));
        }
        if (row - 1 >= 1 && column - 2 >= 'A') {
            attackedSquares.add((char) (column - 2) + "" + (row - 1));
        }
        if (row - 1 >= 1 && column + 2 <= 'H') {
            attackedSquares.add((char) (column + 2) + "" + (row - 1));
        }
        attackedSquares.remove(knight);
        return attackedSquares;
    }
}