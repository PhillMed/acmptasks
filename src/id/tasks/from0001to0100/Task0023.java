package id.tasks.from0001to0100;

/*
"Fortune-telling"
it is necessary to calculate the sum of all the numbers by which n is divisible without remainder.
*/

import java.util.Scanner;

public class Task0023 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int sum = 0;
        for (int i = 1; i <= n; i++) {
            if (n  % i == 0) {
                sum += i;
            }
        }
        System.out.println(sum);
    }
}
