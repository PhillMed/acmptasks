package id.tasks.from0001to0100;

/*
"Salary"
It is required to determine: how much the salary of the highest paid of them differs from the lowest paid.
there are always only 3 employees
 */

import java.util.Scanner;

public class Task0021 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] arrSal = new int[3];
        for (int i = 0; i < arrSal.length; i++) {
            arrSal[i] = sc.nextInt();
        }
        System.out.println(Math.max(arrSal[0], Math.max(arrSal[1], arrSal[2])) - Math.min(arrSal[0], Math.min(arrSal[1], arrSal[2])));
    }
}

