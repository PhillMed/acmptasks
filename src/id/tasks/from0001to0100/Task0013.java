package id.tasks.from0001to0100;

import java.util.Scanner;

/*
"Bulls and cows"
There are two four-digit numbers.
Matching numbers in the same place are called "Bulls"
Matching numbers out of place are called "Cows"
 */

public class Task0013 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int a = sc.nextInt();
        int b = sc.nextInt();

        int[] firstNumber = new int[4];
        int[] secondNumber = new int[4];

        for (int i = 0; i < 4; i++) {
            firstNumber[i] = a % 10;
            a /= 10;
        }
        for (int i = 0; i < 4; i++) {
            secondNumber[i] =  b % 10;
            b /= 10;
        }

        int bulls = 0;
        int cows = 0;

        for (int i = 0; i < 4; i++) {
            if (firstNumber[i] == secondNumber[i]) {
                bulls++;
            }
        }

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (firstNumber[i] == secondNumber[j] && i != j) {
                    cows++;
                }
            }
        }

        System.out.println(bulls + "  " + cows);
    }
}
