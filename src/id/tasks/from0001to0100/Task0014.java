package id.tasks.from0001to0100;

/*
"SCN"
the smallest common multiple of the numbers a and b.
 */

import java.util.Scanner;

public class Task0014 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        for (int i = a; i <= b * a; i++) {
            if (i % a == 0 && i % b == 0) {
                System.out.println(i);
                break;
            }
        }
    }
}
