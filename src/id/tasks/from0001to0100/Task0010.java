package id.tasks.from0001to0100;

// "The equation"
// A*X^3 + B*X^2 + C*X + D = 0
// all equation roots are integers and are on the interval [-100, 100]
// find A B C D by brute-force method

import java.util.Scanner;

public class Task0010 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();
        int d = sc.nextInt();

        int x1 = -1, x2 = -1, x3 = -1;
        for (int i = -100; i <= 100; i++) {
            if (a * i * i * i + b * i * i + c * i + d == 0) {
                if (x1 == -1) x1 = i;
                else if (x2 == -1) x2 = i;
                else {
                    x3 = i;
                    break;
                }
            }
        }
        System.out.println(x1 + " " + x2 + " " + x3);
    }
}