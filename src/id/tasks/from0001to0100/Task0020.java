package id.tasks.from0001to0100;

/*
"Sawtooth sequence"
A sawtooth sequence is a sequence of numbers in which the conditions "more" - "less" are constantly alternating
for example: 5 2 3 1 7 0 4 -1 2.
It is required to find the maximum length of such a sequence from a set of numbers.
N is the number of numbers.
 */

import java.util.Scanner;

public class Task0020 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        System.out.println(comboLessMore(n, arr));
    }

    public static int comboLessMore(int n, int[] arr) {
        int answer = 1;
        int max = 1;
        for (int i = 1; i < n; i++) {
            if (i % 2 == 0) {
                if (arr[i - 1] < arr[i]) {
                    max++;
                    if (answer < max) {
                        answer = max;
                    }
                } else {
                    max = 1;
                }
            } else {
                if (arr[i - 1] > arr[i]) {
                    max++;
                    if (answer < max) {
                        answer = max;
                    }
                } else {
                    max = 1;
                }
            }
        }
        return answer;
    }
}
