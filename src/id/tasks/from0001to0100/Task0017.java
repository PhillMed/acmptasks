package id.tasks.from0001to0100;

/*
"Field of Miracles"
what is the smallest number of sectors that can be on the reel?
N is the number of numbers;
the last sector coincides with the first;

In:
13
5 3 1 3 5 2 5 3 1 3 5 2 5
Out: 6;
 */

import java.util.Scanner;

public class Task0017 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] numbers = new int[n];
        int count = 0;
        for (int i = 1; i <= n; i++) {
            numbers[i - 1] = sc.nextInt();
        }
        for (int i = 0; i < n - 1; i++) {
            if (numbers[0] == numbers[i] && numbers[1] == numbers[i + 1]) {
                count++;
            }
        }
        System.out.println((n - 1) / count);
    }
}
