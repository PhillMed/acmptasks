package id.tasks.from0001to0100;

/*
"Computer game"
the hero jumps on platforms that hang in the air.
when jumping from one platform to the next, the hero loses | y2-y1| units of energy, where y1 and y2 are the heights at which these platforms are located.
there is a super technique that allows you to jump over the platform, but it takes 3*|y3-y1| units of energy
find out what is the minimum amount of energy the hero will need to get from the first platform to the last?

in:         in2:
3           3
1 5 10      1 5 2
Out:        out2:
9           3
 */

import java.util.Arrays;
import java.util.Scanner;

public class Task0029 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] h = new int[n];
        for (int i = 0; i < n; i++) {
            h[i] = sc.nextInt();
        }
        int[] energy = new int[n];
        Arrays.fill(energy, Integer.MAX_VALUE);
        energy[0] = 0;
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n && j <= i + 2; j++) {
                int cost = Math.abs(h[i] - h[j]);
                if (j == i + 1) {
                    energy[j] = Math.min(energy[j], energy[i] + cost);
                } else {
                    energy[j] = Math.min(energy[j], energy[i] + 3 * cost);
                }
            }
        }
        System.out.println(energy[n - 1]);
    }
}