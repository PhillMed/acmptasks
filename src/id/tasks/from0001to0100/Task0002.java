package id.tasks.from0001to0100;

// the sum of the numbers from 1 to N (arithmetic progression)

public class Task0002 {
    public static void main(String[] args) {
        int n = 14;
        System.out.println(n * (n + 1) / 2);
    }
}
