package id.tasks.from0001to0100;

/*
"Symmetry"
Let there be a straight line L and a point A on the plane.
A point B is called a symmetric point A with respect to a straight line L
if the segment AB is perpendicular to the straight line L and is divided in half by the intersection point with it.
!Given a straight line L parallel to one of the coordinate axes, and A point A. Find a point B symmetric to A with respect to L.!

Input data
The first line of the input data contains 4 numbers: x1, y1, x2, y2 are the coordinates of two different points through which the L line passes.
The second line of input data contains 2 numbers xA and yA – coordinates of point A.
All numbers in the input file are integers and do not exceed 10^8 modulo.In particular, if point A lies on the line L, then point B coincides with point A.

Output data
print the numbers xB and yB – coordinates of point B.
 */

import java.util.Scanner;

public class Task0028 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x1 = sc.nextInt();
        int y1 = sc.nextInt();
        int x2 = sc.nextInt();
        int y2 = sc.nextInt();
        int xA = sc.nextInt();
        int yA = sc.nextInt();

        int xB, yB;


        if (x1 == x2) { // Линия параллельна оси y
            xB = 2 * x1 - xA;
            yB = yA;
        } else { // Линия параллельна оси x
            xB = xA;
            yB = 2 * y1 - yA;
        }

        System.out.println(xB + " " + yB);
    }
}
