package id.tasks.from0001to0100;

// "Arithmetic"
// there are three natural numbers A, B and C separated by a space. The numbers A and B are ≤ 10^2, and C is ≤ 10^6
// if A*B=C then YES. Otherwise - NO

import java.util.Arrays;
import java.util.Scanner;

public class Task0008 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter 3 natural numbers separated by a space 'A', 'B' and 'C'" +
                " (A and B should be no more than 100, and C should be no more than 10^6):");
        String string = scanner.nextLine();

        if (!string.matches("[0-9]+")) {
            System.out.println("Invalid format");
        } else {
            int[] numbers = Arrays.stream(string.split("\\s"))
                    .mapToInt(Integer::parseInt)
                    .toArray();

            if (numbers[0] > 100 || numbers[1] > 100 || numbers[2] > 1000000 || numbers.length != 3) {
                System.out.println("Invalid format");
            } else {
                if (numbers[0] * numbers[1] == numbers[2]) {
                    System.out.println("YES");
                } else {
                    System.out.println("NO");
                }
            }
        }
    }
}