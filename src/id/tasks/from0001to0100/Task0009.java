package id.tasks.from0001to0100;

// "Homework"
// the number N is the number of array elements
// there are a total of N integers representing a given array
// N does not exceed 10^2 in absolute value.
// the minimum and maximum elements occur in a given set of numbers only once and are not adjacent.

// it is necessary to find the sum of all positive elements in a given set of integers, then find where the maximum and minimum elements are in a given sequence
// and calculate the product of the numbers located in this sequence between them.

import java.util.Random;
import java.util.stream.IntStream;

public class Task0009 {
    public static void main(String[] args) {
        Random random = new Random();
        int N = random.nextInt(100) + 1;

        int countPositive = 0;
        int sumOfPositive = 0;
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        int indexMin, indexMax;
        int sumArray;

        System.out.println(N);

        int[] numbers = new int[N];
        for (int i = 0; i < N; i++) {
            numbers[i] = random.nextInt(N * 2) - N;

            if (numbers[i] < min) {
                min = numbers[i];
            }

            if (numbers[i] > max) {
                max = numbers[i];
            }

            if (numbers[i] > 0) {
                countPositive++;
                sumOfPositive += numbers[i];
            }

            System.out.print(numbers[i] + " ");
        }
        System.out.println();
        System.out.println("Positive numbers: " + countPositive + "\n" +
                "Sum of positive numbers: " + sumOfPositive);
        System.out.println("Min and max: " + min + " " + max);

        indexMin = findIndex(numbers, min);
        indexMax = findIndex(numbers, max);

        if (indexMax > indexMin) {
            sumArray = sumFromTo(numbers, indexMin, indexMax);
            System.out.println("Sum from " + (indexMin + 1) + " to " + (indexMax + 1) + " elements is: " + sumArray);
        } else {
            sumArray = sumFromTo(numbers, indexMax, indexMin);
            System.out.println("Sum from " + (indexMax + 1) + " to " + (indexMin + 1) + " elements is: " + sumArray);
        }
    }

    private static int findIndex(int[] arr, int val) {
        return IntStream.range(0, arr.length).filter(i -> arr[i] == val).findFirst().orElse(-1);
    }

    private static int sumFromTo(int[] arr, int firstIndex, int secondIndex) {
        int sum = 0;
        for (int i = firstIndex+1; i < secondIndex; i++) {
            sum += arr[i];
        }
        return sum;
    }
}
