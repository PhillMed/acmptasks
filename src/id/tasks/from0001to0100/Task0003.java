package id.tasks.from0001to0100;

// a special method of multiplication by 5

public class Task0003 {
    public static void main(String[] args) {
        int a = 125;
        int b = a / 10;
        System.out.println(b * (b + 1) + "25"); // 125*5 = 12*13 + "25"
    }
}
