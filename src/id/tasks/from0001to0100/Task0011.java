package id.tasks.from0001to0100;

// "Bunny"
// how many ways does the bunny have to get to the top of the ladder with the given values of K and N.
// N - number of stairs
// K - maximum number of stairs at a time

import java.util.Scanner;

public class Task0011 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int k = sc.nextInt();
        int n = sc.nextInt();
        int[] ways = new int[n + 1];
        ways[0] = 1;
        ways[1] = 1;
        for (int i = 2; i <= n; i++) {
            for (int j = 1; j <= k && i - j >= 0; j++) {
                ways[i] += ways[i - j];
            }
        }
        System.out.println(ways[n]);
    }
}

