package id.tasks.from0001to0100;

/*
"Clock"
The first and second lines contain the beginning and end of the time interval, respectively
it is necessary to calculate how many times each digit occurs in a given time interval
The output should contain 10 lines (digits 0-9)
in:
23:59:58
23:59:59
out:
0
0
2
2
0
4
0
0
1
3
 */

import java.util.Scanner;

public class Task0030 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String start = scanner.next();
        String end = scanner.next();

        int[] counts = new int[10];

        int s = toSeconds(start);
        int e = toSeconds(end);

        for (int i = s; i <= e; i++) {
            int h = i / 3600;
            int m = (i % 3600) / 60;
            int sec = i % 60;

            counts[h / 10]++;
            counts[h % 10]++;
            counts[m / 10]++;
            counts[m % 10]++;
            counts[sec / 10]++;
            counts[sec % 10]++;
        }

        for (int i = 0; i < 10; i++) {
            System.out.println(counts[i]);
        }
    }

    private static int toSeconds(String time) {
        String[] parts = time.split(":");
        int hours = Integer.parseInt(parts[0]);
        int minutes = Integer.parseInt(parts[1]);
        int seconds = Integer.parseInt(parts[2]);
        return hours * 3600 + minutes * 60 + seconds;
    }
}