package id.tasks.from0001to0100;

/*
"two circles"
Two circles are given on the plane. It is required to check whether they have at least one common point.
 */

import java.util.Scanner;

public class Task0026 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x1 = sc.nextInt();
        int y1 = sc.nextInt();
        int r1 = sc.nextInt();
        int x2 = sc.nextInt();
        int y2 = sc.nextInt();
        int r2 = sc.nextInt();
        boolean isPoint = checkPoint(x1, y1, r1, x2, y2, r2);
        if (isPoint) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }
    }

    public static boolean checkPoint(int x1, int y1, int r1, int x2, int y2, int r2) {
        double distance = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
        return distance <= r1 + r2;
    }
}
