package id.tasks.from0001to0100;

//"Statistics"
// we have an array with the number of numbers from 1 to 100
// this number can be from 1 do 31
// it is necessary to divide the array into even and odd numbers
// if even numbers >= odd numbers, then print "YES". otherwise, "NO"

import java.util.Random;

public class Task0005 {
    public static void main(String[] args) {
        Random random = new Random();
        int numberEven = 0, numberOdd = 0;

        int count = random.nextInt(100) + 1;
        System.out.println("total numbers: " + count);

        int[] numbers = new int[count];
        System.out.println("Array of numbers:");
        for (int i = 0; i < count; i++) {
            numbers[i] = random.nextInt(31) + 1;
            System.out.print(numbers[i] + " ");
        }
        System.out.print("\nEven numbers: ");
        for (int i = 0; i < count; i++) {
            if (numbers[i] % 2 == 0) {
                System.out.print(numbers[i] + " ");
                numberEven++;
            }
        }
        System.out.println("(" + numberEven + ")");
        System.out.print("Odd numbers: ");
        for (int i = 0; i < count; i++) {
            if (numbers[i] % 2 != 0) {
                System.out.print(numbers[i] + " ");
                numberOdd++;
            }
        }
        System.out.println("(" + numberOdd + ")");
        if (numberEven >= numberOdd) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }
}
