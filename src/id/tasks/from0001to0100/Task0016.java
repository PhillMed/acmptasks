package id.tasks.from0001to0100;

/*
"ladder"
"a program that calculates the number of ladders that can be built from N cubes"
"A ladder is a set of cubes in which each upper layer contains fewer cubes than the previous one"
input: 3, out: 2; input: 6, out: 4;
 */

import java.util.Scanner;

public class Task0016 {
    public static int countLadders(int n, int level) {
        if (n == 0) {
            return 1;
        }
        int count = 0;
        for (int i = level; i <= n; i++) {
            count += countLadders(n - i, i + 1);
        }
        return count;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int count = countLadders(n, 1);
        System.out.println(count);
    }
}