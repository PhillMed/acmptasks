package id.tasks.from0001to0100;

/*
"Units"
Find the number of units in the binary notation of a given number.
n: (0 ≤ n ≤ 2×10^9)
*/

import java.util.Scanner;

public class Task0022 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int count = 0;
        while (n != 0) {
            if (n % 2 == 1) {
                count++;
            }
            n /= 2;
        }
        System.out.println(count);
    }
}
