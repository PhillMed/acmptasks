package id.tasks.from0001to0100;

// "Chess"
// we need to make sure that the knight's move is correct
// we have a chess board with numbers A1 - H8
// if the code is correct, output YES. Otherwise, NO
// the format of the move looks like this: C7-D5 (YES)
// If the coordinates are not defined or set incorrectly, then output the message "ERROR".

import java.util.Scanner;

public class Task0006 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("enter the knight's move in the format 'b1-c3'");
        String currentPosition;
        String nextPosition;
        String move = scanner.next();
        if (isRightStringFormat(move)) {
            currentPosition = move.split("-")[0].toLowerCase();
            nextPosition = move.split("-")[1].toLowerCase();
            if (isRightMove(currentPosition.charAt(0), Character.digit(currentPosition.charAt(1), 10),
                    nextPosition.charAt(0), Character.digit(nextPosition.charAt(1), 10)))
                System.out.println("YES");
            else System.out.println("NO");
        } else {
            System.out.println("ERROR");
        }
    }

    private static boolean isRightStringFormat(String inputData) {                        //check for chess format
        return (inputData.matches("([a-h])([1-8])(-)([a-h])([1-8])"));              //from a to h and from 1 to 8
    }
    
    private static boolean isRightMove(char letCurrPos, int numCurrPos, char letNewPos, int numNewPos) {
        return (Math.abs(letCurrPos - letNewPos) + Math.abs(numCurrPos - numNewPos) == 3 &&
                letCurrPos != letNewPos && numCurrPos != numNewPos);
    }
}
