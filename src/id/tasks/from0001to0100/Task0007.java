package id.tasks.from0001to0100;

//"Gold of the ABBA tribe", Long arithmetic
// we have 3 random numbers from 1 to 10^100
// we need to find out which number is greater

import java.util.Scanner;

public class Task0007 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("num1=");
        String num1 = scanner.next();

        System.out.print("num2=");
        String num2 = scanner.next();

        System.out.print("num3=");
        String num3 = scanner.next();

        if (isBadStringFormat(num1) || isBadStringFormat(num2) || isBadStringFormat(num3)) {
            System.out.println("Error. You have gone beyond the task condition!");
        } else {
            String res = max(num1, max(num2, num3));
            System.out.println(res);
        }
    }

    public static String max(String n1, String n2) {
        if (n1.length() > n2.length())
            return n1;
        else if (n1.length() < n2.length())
            return n2;
        else
            for (int i = 0; i < n1.length(); i++) {
                if (n1.charAt(i) > n2.charAt(i))
                    return n1;
                else if (n1.charAt(i) < n2.charAt(i))
                    return n2;
            }
        return n1;
    }

    private static boolean isBadStringFormat(String inputData) {
        return ((!inputData.matches("[0-9]+")) || inputData.length() > 1000);
    }
}
