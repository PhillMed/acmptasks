package id.tasks.from0001to0100;

/*
"Painter"
there is a white canvas shaped like a rectangle with a width w and a height h.
there are n rectangles on this canvas with sides parallel to the sides of the canvas and vertices located in integer coordinates.
determine the area of the unpainted part of the canvas.
 */

import java.util.Scanner;

public class Task0027 {
    public static void main(String[] args) {
        Scanner sc  = new Scanner(System.in);
        int w = sc.nextInt();
        int h = sc.nextInt();
        int n = sc.nextInt();
        int[][] rects = new int[n][4];
        for (int i = 0; i < n; i++) {
            rects[i][0] = sc.nextInt(); // left
            rects[i][1] = sc.nextInt(); // bottom
            rects[i][2] = sc.nextInt(); // right
            rects[i][3] = sc.nextInt(); // top
        }
        int area = getUnpaintedArea(w, h, rects);
        System.out.println(area);
    }

    private static int getUnpaintedArea(int width, int height, int[][] rects) {
        boolean[][] painted = new boolean[width][height];
        for (int[] rect : rects) {
            int left = rect[0];
            int bottom = rect[1];
            int right = rect[2];
            int top = rect[3];
            for (int i = left; i < right; i++) {
                for (int j = bottom; j < top; j++) {
                    painted[i][j] = true;
                }
            }
        }
        int unpaintedArea = 0;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (!painted[i][j]) {
                    unpaintedArea++;
                }
            }
        }
        return unpaintedArea;
    }
}