package id.tasks.from0001to0100;

/*
"Cutting down trees"
there are N trees growing in total, the distances between neighboring trees are the same.
there should be M trees left, and the distances between neighboring trees should be the same.
    a program that, based on the given numbers N and M,
    will determine how many ways there are to cut down some of the N trees
    so that after cutting down there are m trees and neighboring trees are at an equal distance from each other.
 */

import java.util.Scanner;

public class Task0024 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();

        System.out.println(variants(n, m));
    }

    public static int variants(int all_trees, int remaining_trees) {
        if (0 == remaining_trees)
            return 1;
        if (1 == remaining_trees)
            return all_trees;
        if (all_trees < remaining_trees)
            return 0;

        int variants = 0;
        for (int distance = 0; true; distance++) {
            int usedPositions = remaining_trees + distance * (remaining_trees - 1);
            if (usedPositions > all_trees)
                break;
            variants += all_trees - usedPositions + 1;
        }
        return variants;
    }
}
