package id.tasks.from0001to0100;

/*
"Summer residents"
3 - N, number of summer residents
(X,Y) – coordinates of the parachutist's landing
(X1, Y1, X2, Y2, X3, Y3, X4, Y4) = rectangle coordinate format

input - X Y X1 Y1 X2 Y2 X3 Y3 X4 Y4
6 6 3 6 6 9 8 7 5 4 - coordinates for 1
13 5 9 2 9 8 12 8 12 2 - coordinates for 2
3 2 2 1 2 3 6 3 6 1 - coordinates for 3
 */

import java.util.Scanner;

public class Task0012 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int count = 0;
        for (int i = 0; i < n; i++) {
            int writeX = 0;
            int writeY = 0;
            int[] x = new int[5];
            int[] y = new int[5];
            for (int j = 0; j < 10 ; j++) {
                if (j % 2 == 0) {
                    x[writeX] = sc.nextInt();
                    writeX++;
                } else {
                    y[writeY] = sc.nextInt();
                    writeY++;
                }
            }
            int xMax = Math.max(x[1], Math.max(x[2], Math.max(x[3], x[4])));
            int xMin = Math.min(x[1], Math.min(x[2], Math.min(x[3], x[4])));
            int yMax = Math.max(y[1], Math.max(y[2], Math.max(y[3], y[4])));
            int yMin = Math.min(y[1], Math.min(y[2], Math.min(y[3], y[4])));
            if (x[0] <= xMax  && x[0] >= xMin && y[0] <= yMax && y[0] >= yMin) {
                count++;
            }
        }
        System.out.println(count);
    }
}
