package id.tasks.from0001to0100;

/*
"Factorial"
The factorial of a number is the product of all positive integers less than or equal to the number.
N - the number;
 */

import java.math.BigInteger;
import java.util.Scanner;

public class Task0018 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        BigInteger factorial = BigInteger.ONE;
        for (int i = 1; i <= n; i++) {
            factorial = factorial.multiply(BigInteger.valueOf(i));
        }
        System.out.println(factorial);
    }
}